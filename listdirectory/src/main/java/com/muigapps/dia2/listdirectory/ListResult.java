package com.muigapps.dia2.listdirectory;


import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;

import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

/**
 * La anotación de Mojo es la indicada por Maven para declarar el nombre del Goal y el ciclo de vida donde se debe
 * ejecutar.
 * EL parametro name indica el nombre del goal
 * El parametro defaultPhase indica la fase del ciclo de vida donde se debe ejecutar.
 * 
 * En este caso a la hora de incluirlo en un proyecto se ejecutara automáticamente en el momento de package, pero
 * tambien se podría ejecutar de forma directa como mvn listdirectory:listresult donde el primer listresult es el nombre
 * del artifactId de este plugin y listresult es el goal definido en esta anotación
 */
@Mojo( name = "listresult", defaultPhase = LifecyclePhase.PACKAGE )
public class ListResult
    extends AbstractMojo
{
 
	/** Es el parametro de entrada donde indicamos el directorio a leer, en este caso, si no se indica lo contrario
	 * se leera el directorio de salida de compilación del proyecto
	 */
    @Parameter( defaultValue = "${project.build.directory}", property = "outputDir", required = true )
    private File outputDirectory;

    
    /**
     * Este será el metodo que llame maven cuando necesite ejecutar el pugin
     */
    public void execute() throws MojoExecutionException
    {
    	
      /**
       * El metodo getLog() es un método que heredamos de AbstractMojo y nos permite 
       * escribir el resultado o lo que necesitemos por consola
       */
      getLog().info("----------------------------------------------");
      getLog().info("SE INICIA LA EJECUCIÓN DE LIST RESULT");
      getLog().info("----------------------------------------------");
      
      /**
       * Validamos que el directorio exista y es un directorio
       */
      if(outputDirectory != null && outputDirectory.exists() && outputDirectory.isDirectory()) {
    	  readDirectory(outputDirectory, "+");
    	  
      } else {
    	  /** En caso de lanzar esat excepción, maven cortara la ejecución, en caso de lanzarse dentro
    	   * de un ciclo de vida, se parara, pro ejemplo en caso de hacer mvn install no se acabara de 
    	   * instalar en el repositorio local
    	   */
    	  throw new MojoExecutionException("EL directorio no fue indicando, no existe o no es un directorio");
      }
    }
    
    
    /**
     * Metodo recursivo para leer un directorio
     * 
     * @param directory ==> Directorio a leer
     * @param textInitial ==> Cadena de texto a poner antes que el nombre del fichero así podemos dibujar un arbol
     * 
     */
    private void readDirectory(File directory, String textInitial) {
    	if(directory != null && directory.exists() && directory.isDirectory()) {
    		for(final File fileEntry: directory.listFiles()) {
    			if(!fileEntry.isDirectory()) {
    				getLog().info(textInitial + "- " + fileEntry.getName());
    			} else {
    				getLog().info(textInitial + "+ " + fileEntry.getName());
    				readDirectory(fileEntry,textInitial + "-");
    			}
    		}
    	}
    }
}
