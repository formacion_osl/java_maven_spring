package com.muigapps.curso2022.proyecto.config;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

@Component
public class AuditFilter extends OncePerRequestFilter{

	
	
	@Override
	protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
			throws ServletException, IOException {
		
		String url = request.getRequestURI();
		if(request.getQueryString() != null && !request.getQueryString().isEmpty()) {
			url = url + request.getQueryString();
		}
		
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
		
		System.out.println("NUEVA PETICION " + url + " RESPUESTA FUE " + response.getStatus() + " HORA: " + format.format(new Date()));
		
		
		response.addHeader("curso", "mavenspring2022");
		
//		if(request.getHeader("app") != null && request.getHeader("app").equals("curso")) {
//			filterChain.doFilter(request, response);
//		}
		
		filterChain.doFilter(request, response);
		
	}

}
