package com.muigapps.curso2022.proyecto.ws;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.muigapps.curso2022.proyecto.core.manager.MedicationManager;
import com.muigapps.curso2022.proyecto.core.model.Appointment;
import com.muigapps.curso2022.proyecto.core.model.Medication;
import com.muigapps.curso2022.proyecto.ws.model.Response;
import com.muigapps.curso2022.proyecto.ws.model.ResponseList;

@RestController
@RequestMapping("/medication")
public class MedicationController {
	
	@Autowired
	private MedicationManager manager;
	
	@RequestMapping(path = {"/all", "/all/"}, method = RequestMethod.GET, produces = {"application/json"})
	public Response<List<Medication>> getAll(){
		List<Medication> medications = manager.findAll();
		
		return new Response<List<Medication>>(200,"OK", medications);
	}
	
	
	@RequestMapping(path = {"/page/{page}/{size}","/page/{page}/{size}/"}, method = RequestMethod.GET, produces= {"application/json", "application/xml"})
	public ResponseList<Medication> getAll(@PathVariable("page")Integer page,@PathVariable("size")Integer size ){
		Page<Medication> appointments = manager.findAll(page,size);
		
		return new ResponseList<Medication>(200, "Ok", appointments);
	}
	
	@RequestMapping(path = {"/one/{id}","/one/{id}/"}, method = RequestMethod.GET, produces = {"application/json"})
	public Response<Medication> findOne(@PathVariable("id") Integer id){
		Medication medication = manager.findOne(id);
		
		return new Response<Medication>(200, "ok", medication);
		
	}
	
	@RequestMapping(path = {"/one","/one/"}, method = RequestMethod.GET, produces = {"application/json"})
	public Response<Medication> findOneRequest(@RequestParam("id") Integer id){
		Medication medication = manager.findOne(id);
		
		return new Response<Medication>(200, "ok", medication);
		
	}
	
	@RequestMapping(path = {"/onerequest","/onerequest/"}, method = RequestMethod.GET, produces = {"application/json"})
	public Response<Medication> findOneRequest(HttpServletRequest request) throws Exception{
		try {
			Medication medication = manager.findOne(Integer.valueOf(request.getParameter("id")));
			
			return new Response<Medication>(200, "ok", medication);
		} catch (Exception e) {
			throw new Exception("El atributo no tiene el formato correcto");
		}
		
	}
	
	@RequestMapping(path = {"", "/"}, method = RequestMethod.POST, consumes = {"application/json"}, produces = {"application/json"})
	public Response<Medication> save(@RequestBody Medication medication) throws Exception{
		if(medication.getId() != null) {
			throw new Exception("Mediante  post solo se puede crear nuevos objetos y no actualizarlos");
		}
		Medication result = manager.save(medication);
		
		return new Response<Medication>(200, "ok", result); 
	}
	
	
	@RequestMapping(path = {"/one/{id}", "/one/{id}/"}, method = RequestMethod.PUT, consumes = {"application/json"}, produces = {"application/json"})
	public Response<Medication> update(@PathVariable("id") Integer id, @RequestBody Medication medication) throws Exception{
		
		medication.setId(id);
		Medication result = manager.save(medication);
		
		return new Response<Medication>(200, "ok", result); 
	}
	
	@RequestMapping(path = {"/one/{id}", "/one/{id}/"}, method = RequestMethod.DELETE, produces = {"application/json"})
	public Response<Boolean> delete(@PathVariable("id") Integer id){
		Boolean result = manager.delete(id);
		
		return new Response<Boolean>(200, "Ok", result);
	}
	
	


}
