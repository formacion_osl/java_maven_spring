package com.muigapps.curso2022.proyecto.ws.soap.impl;

import java.util.ArrayList;
import java.util.List;

import javax.jws.WebService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.muigapps.curso2022.proyecto.core.manager.AppointmentManager;
import com.muigapps.curso2022.proyecto.core.model.Appointment;
import com.muigapps.curso2022.proyecto.ws.model.Response;
import com.muigapps.curso2022.proyecto.ws.model.ResponseList;
import com.muigapps.curso2022.proyecto.ws.soap.AppointmentWSSoap;

@Service
@WebService(name = "appointmentwssoap", endpointInterface = "com.muigapps.curso2022.proyecto.ws.soap.AppointmentWSSoap")
public class AppointmentSoapWSImpl implements AppointmentWSSoap{

	@Autowired
	private AppointmentManager manager;
	
	@Override
	public ResponseList<Appointment> findAll() {
		List<Appointment> result = manager.findAll();
		return new ResponseList<Appointment>(200, "ok", new ArrayList<Appointment>(result));
	}

	@Override
	public Response<Appointment> save(Appointment appointment) {
		Appointment result = manager.save(appointment);
		return new Response<Appointment>(200, "ok", result);
	}

	@Override
	public Response<Appointment> getOne(Integer id) {
		Appointment result = manager.findOne(id);
		return new Response<Appointment>(200, "ok", result);
	}

	@Override
	public Response<Appointment> update(Integer id, Appointment appointment) {
		appointment.setId(id);
		Appointment result = manager.save(appointment);
		return new Response<Appointment>(200, "ok", result);
	}

	@Override
	public Response<Boolean> delete(Integer id) {
		Boolean result = manager.delete(id);
		return new Response<Boolean>(200, "ok", result);
	}

}
