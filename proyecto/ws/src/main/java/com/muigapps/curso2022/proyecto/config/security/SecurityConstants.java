package com.muigapps.curso2022.proyecto.config.security;

public class SecurityConstants {
	
	public static String HEADER_AUTHORIZACION_KEY = "Authorization";
	public static String TOKEN_BEARER_PREFIX = "Bearer";
	public static Long TOKEN_EXPIRATION_TIME = 86400000l;
	public static String SUPER_SECRET_KEY = "CURSO.2022.2022.10.10";
	public static String TOKEN_PREFIX_POSITION = "CURSO-";
	

}
