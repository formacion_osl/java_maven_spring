package com.muigapps.curso2022.proyecto.config.security;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import java.util.TimeZone;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.muigapps.curso2022.proyecto.config.security.model.UserData;
import com.muigapps.curso2022.proyecto.core.repository.UserRepository;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

public class JWTAuthenticationFilter extends UsernamePasswordAuthenticationFilter {

	private AuthenticationManager authenticationManager;

	private UserRepository userRepository;

	public JWTAuthenticationFilter(AuthenticationManager authenticationManager,UserRepository userRepository) {
		this.authenticationManager = authenticationManager;
		this.userRepository = userRepository;
	}
	

	@Override
	public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response)
			throws AuthenticationException {
		try {
			com.muigapps.curso2022.proyecto.core.model.User credenciales = new ObjectMapper().readValue(request.getInputStream(), com.muigapps.curso2022.proyecto.core.model.User.class);

			return authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(
					credenciales.getEmail(), credenciales.getPass(), getPermissions(credenciales)));
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	protected void successfulAuthentication(HttpServletRequest request, HttpServletResponse response, FilterChain chain,
			Authentication auth) throws IOException, ServletException {
		com.muigapps.curso2022.proyecto.core.model.User user = userRepository.findOneByEmail(((User)auth.getPrincipal()).getUsername());
		
		String infouser = new ObjectMapper().writeValueAsString(new UserData(user));
		String token = Jwts.builder().setIssuedAt(new Date()).setIssuer(infouser)
				.setSubject(((User)auth.getPrincipal()).getUsername())
				.setExpiration(new Date(System.currentTimeMillis() + SecurityConstants.TOKEN_EXPIRATION_TIME))
				.signWith(SignatureAlgorithm.HS512, SecurityConstants.SUPER_SECRET_KEY).compact();
		
		response.addHeader(SecurityConstants.HEADER_AUTHORIZACION_KEY, SecurityConstants.TOKEN_BEARER_PREFIX + " " + token);
		response.setContentType(MediaType.APPLICATION_JSON_VALUE);
		
		UserData userData = new UserData(user);
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
		format.setTimeZone(TimeZone.getTimeZone("UTC"));
		response.getWriter().write(new ObjectMapper().setDateFormat(format).writeValueAsString(new ReponseLogin(token, userData)));
		
	}
	
	private Set<GrantedAuthority> getPermissions(com.muigapps.curso2022.proyecto.core.model.User user){
		Set<GrantedAuthority> permissions = new HashSet<>();
		
		if(user.getRol() != null && user.getRol().getCode() != null) {
				permissions.add(new SimpleGrantedAuthority(user.getRol().getCode()));
		}
		
		return permissions;
	}

	
	
	public static class ReponseLogin {
		private String token;
		private UserData user;
		public String getToken() {
			return token;
		}
		public void setToken(String token) {
			this.token = token;
		}
		public UserData getUser() {
			return user;
		}
		public void setUser(UserData user) {
			this.user = user;
		}
		public ReponseLogin(String token, UserData user) {
			super();
			this.token = token;
			this.user = user;
		}
		public ReponseLogin() {
			super();
		}
		
	}
	

}