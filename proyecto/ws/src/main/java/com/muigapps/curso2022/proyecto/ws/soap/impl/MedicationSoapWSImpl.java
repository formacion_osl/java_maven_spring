package com.muigapps.curso2022.proyecto.ws.soap.impl;

import java.util.ArrayList;
import java.util.List;

import javax.jws.WebService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.muigapps.curso2022.proyecto.core.manager.MedicationManager;
import com.muigapps.curso2022.proyecto.core.model.Medication;
import com.muigapps.curso2022.proyecto.ws.model.Response;
import com.muigapps.curso2022.proyecto.ws.model.ResponseList;
import com.muigapps.curso2022.proyecto.ws.soap.MedicationSoapWS;

@Service
@WebService(name = "clientwssoap", endpointInterface = "com.muigapps.curso2022.proyecto.ws.soap.MedicationSoapWS")
public class MedicationSoapWSImpl implements MedicationSoapWS {

	@Autowired
	private MedicationManager medicationManager;
	
	@Override
	public ResponseList<Medication> findAll() {
		List<Medication> result = medicationManager.findAll();
		return new ResponseList<Medication>(200, "ok", new ArrayList<Medication>(result));
	}

	@Override
	public Response<Medication> save(Medication medication) {
		Medication result = medicationManager.save(medication);
		return new Response<Medication>(200, "ok", result);
	}

	@Override
	public Response<Medication> getOne(Integer id) {
		Medication result = medicationManager.findOne(id);
		return new Response<Medication>(200, "ok", result);
	}

	@Override
	public Response<Medication> update(Integer id, Medication medication) {
		medication.setId(id);
		Medication result = medicationManager.save(medication);
		return new Response<Medication>(200, "ok", result);
	}

	@Override
	public Response<Boolean> delete(Integer id) {
		Boolean result = medicationManager.delete(id);
		return new Response<Boolean>(200, "ok", result);
	}

}
