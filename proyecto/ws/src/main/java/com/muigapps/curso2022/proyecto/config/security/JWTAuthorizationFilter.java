package com.muigapps.curso2022.proyecto.config.security;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.HashSet;
import java.util.Set;
import java.util.TimeZone;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.muigapps.curso2022.proyecto.config.security.model.UserData;
import com.muigapps.curso2022.proyecto.core.model.User;
import com.muigapps.curso2022.proyecto.core.repository.UserRepository;

import io.jsonwebtoken.Jwts;

public class JWTAuthorizationFilter extends BasicAuthenticationFilter {
	

	private UserRepository userRepository;
	
	public JWTAuthorizationFilter(AuthenticationManager authManager,UserRepository userRepository) {
		super(authManager);
		this.userRepository = userRepository;
	}

	@Override
	protected void doFilterInternal(HttpServletRequest req, HttpServletResponse res, FilterChain chain)
			throws IOException, ServletException {
		String header = req.getHeader(SecurityConstants.HEADER_AUTHORIZACION_KEY);
		if (header == null || !header.startsWith(SecurityConstants.TOKEN_BEARER_PREFIX)) {
			chain.doFilter(req, res);
			return;
		}
		UsernamePasswordAuthenticationToken authentication = getAuthentication(req);
		SecurityContextHolder.getContext().setAuthentication(authentication);
		chain.doFilter(req, res);
	}

	private UsernamePasswordAuthenticationToken getAuthentication(HttpServletRequest request) throws JsonMappingException, JsonProcessingException {
		String token = request.getHeader(SecurityConstants.HEADER_AUTHORIZACION_KEY);
		try {
			if (token != null) {
				// Se procesa el token y se recupera el usuario.
				String user = Jwts.parser()
							.setSigningKey(SecurityConstants.SUPER_SECRET_KEY)
							.parseClaimsJws(token.replace(SecurityConstants.TOKEN_BEARER_PREFIX, ""))
							.getBody()
							.getSubject();
	
				if (user != null) {
					String dataBody = Jwts.parser()
					.setSigningKey(SecurityConstants.SUPER_SECRET_KEY)
					.parseClaimsJws(token.replace(SecurityConstants.TOKEN_BEARER_PREFIX, ""))
					.getBody().getIssuer();
	
					SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
					format.setTimeZone(TimeZone.getTimeZone("UTC"));
					UserData data =  new ObjectMapper().setDateFormat(format).readValue(dataBody, UserData.class);
					
					User userBd = userRepository.findById(data.getId()).orElse(null);
					
					if(userBd != null) {
						data = new UserData(userBd);
					}
					
					return new UsernamePasswordAuthenticationToken(user, data, getPermissions(data));
				}
				return null;
			}
		}catch (Exception e) {
			// TODO: handle exception
		}
		return null;
	}
	

	
	private Set<GrantedAuthority> getPermissions(UserData user){
		Set<GrantedAuthority> permissions = new HashSet<>();
		
		if(user!= null && user.getPermissions() != null) {
			for(String p: user.getPermissions()) {
				permissions.add(new SimpleGrantedAuthority(p));
			}
		}
		
		return permissions;
	}
	


	
	
}