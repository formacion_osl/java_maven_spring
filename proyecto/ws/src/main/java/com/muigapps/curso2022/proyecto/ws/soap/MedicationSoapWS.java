package com.muigapps.curso2022.proyecto.ws.soap;

import javax.jws.WebService;

import com.muigapps.curso2022.proyecto.core.model.Medication;
import com.muigapps.curso2022.proyecto.ws.model.Response;
import com.muigapps.curso2022.proyecto.ws.model.ResponseList;

@WebService
public interface MedicationSoapWS {

	public ResponseList<Medication> findAll();

	public Response<Medication> save(Medication medication);

	public Response<Medication> getOne(Integer id);

	public Response<Medication> update(Integer id, Medication medication);

	public Response<Boolean> delete(Integer id);

}
