package com.muigapps.curso2022.proyecto.ws;

import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.muigapps.curso2022.proyecto.core.manager.AppointmentManager;
import com.muigapps.curso2022.proyecto.core.model.Appointment;
import com.muigapps.curso2022.proyecto.ws.model.Response;
import com.muigapps.curso2022.proyecto.ws.model.ResponseList;

@RestController
@RequestMapping("/appointment")
public class AppointmentController {
	
	@Autowired
	private AppointmentManager manager;
	
	@RequestMapping(path = {"/all", "/all/"}, method = RequestMethod.GET, produces = {"application/json"})
	public Response<List<Appointment>> getAll(){
		List<Appointment> appointments = manager.findAll();
		
		return new Response<List<Appointment>>(200,"OK", appointments);
	}
	
	@RequestMapping(path = {"/dateandname/{date}/{name}", "/dateandname/{date}/{name}/"}, method = RequestMethod.GET, produces = {"application/json"})
	public Response<List<Appointment>> findBeforeDateAndNameClient(@PathVariable("date")@DateTimeFormat(pattern="yyyy-MM-dd") Date date,@PathVariable("name")String nameClient ){
		List<Appointment> appointments = manager.findBeforeDateAndNameClient(date, nameClient);
		
		return new Response<List<Appointment>>(200,"OK", appointments);
	}
	
	
	@RequestMapping(path = {"/page/{page}/{size}","/page/{page}/{size}/"}, method = RequestMethod.GET, produces= {"application/json", "application/xml"})
	public ResponseList<Appointment> getAll(@PathVariable("page")Integer page,@PathVariable("size")Integer size ){
		Page<Appointment> appointments = manager.findAll(page,size);
		
		return new ResponseList<Appointment>(200, "Ok", appointments);
	}
	
	
	@RequestMapping(path = {"/one/{id}","/one/{id}/"}, method = RequestMethod.GET, produces = {"application/json"})
	public Response<Appointment> findOne(@PathVariable("id") Integer id){
		Appointment appointment = manager.findOne(id);
		
		return new Response<Appointment>(200, "ok", appointment);
		
	}
	
	@RequestMapping(path = {"/one","/one/"}, method = RequestMethod.GET, produces = {"application/json"})
	public Response<Appointment> findOneRequest(@RequestParam("id") Integer id){
		Appointment appointment = manager.findOne(id);
		
		return new Response<Appointment>(200, "ok", appointment);
		
	}
	
	@RequestMapping(path = {"/onerequest","/onerequest/"}, method = RequestMethod.GET, produces = {"application/json"})
	public Response<Appointment> findOneRequest(HttpServletRequest request) throws Exception{
		try {
			Appointment appointment = manager.findOne(Integer.valueOf(request.getParameter("id")));
			
			return new Response<Appointment>(200, "ok", appointment);
		} catch (Exception e) {
			throw new Exception("El atributo no tiene el formato correcto");
		}
		
	}
	
	@RequestMapping(path = {"", "/"}, method = RequestMethod.POST, consumes = {"application/json"}, produces = {"application/json"})
	public Response<Appointment> save(@RequestBody Appointment appointment) throws Exception{
		if(appointment.getId() != null) {
			throw new Exception("Mediante  post solo se puede crear nuevos objetos y no actualizarlos");
		}
		Appointment result = manager.save(appointment);
		
		return new Response<Appointment>(200, "ok", result); 
	}
	
	
	@RequestMapping(path = {"/one/{id}", "/one/{id}/"}, method = RequestMethod.PUT, consumes = {"application/json"}, produces = {"application/json"})
	public Response<Appointment> update(@PathVariable("id") Integer id, @RequestBody Appointment appointment) throws Exception{
		
		appointment.setId(id);
		Appointment result = manager.save(appointment);
		
		return new Response<Appointment>(200, "ok", result); 
	}
	
	@RequestMapping(path = {"/one/{id}", "/one/{id}/"}, method = RequestMethod.DELETE, produces = {"application/json"})
	public Response<Boolean> delete(@PathVariable("id") Integer id){
		Boolean result = manager.delete(id);
		
		return new Response<Boolean>(200, "Ok", result);
	}
	
	


}
