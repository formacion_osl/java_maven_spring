package com.muigapps.curso2022.proyecto.config.security.model;

import java.util.ArrayList;

import com.muigapps.curso2022.proyecto.core.model.User;

public class UserData {

	private Long id;
	private String email;
	private String name;
	private String lastname;
	private ArrayList<String> permissions = new ArrayList<>();
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getLastname() {
		return lastname;
	}
	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public ArrayList<String> getPermissions() {
		return permissions;
	}
	public void setPermissions(ArrayList<String> permissions) {
		this.permissions = permissions;
	}
	public UserData() {
		super();
	}
	public UserData(User user) {
		super();
		this.id = user.getId();
		this.email = user.getEmail();
		this.name = user.getName();
		this.lastname = user.getLastname();
		this.permissions = new ArrayList<String>();
		this.permissions.add(user.getRol().getCode() );
	}

	
	
	
}
