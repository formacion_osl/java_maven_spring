package com.muigapps.curso2022.proyecto.ws.soap.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.jws.WebService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.muigapps.curso2022.proyecto.core.manager.ClientManager;
import com.muigapps.curso2022.proyecto.core.model.Client;
import com.muigapps.curso2022.proyecto.ws.model.Response;
import com.muigapps.curso2022.proyecto.ws.model.ResponseList;
import com.muigapps.curso2022.proyecto.ws.soap.ClientSoapWS;

@Service
@WebService(name = "clientsoapws", endpointInterface = "com.muigapps.curso2022.proyecto.ws.soap.ClientSoapWS")
public class ClientSoapWSImpl implements ClientSoapWS{

	@Autowired
	private ClientManager clientManager;
	
	public ClientManager getClientManager() {
		return clientManager;
	}

	public void setClientManager(ClientManager clientManager) {
		this.clientManager = clientManager;
	}

	@Override
	public ResponseList<Client> findAll() {
		List<Client> clients =  clientManager.findAll();
		return new ResponseList<Client>(200, "ok", new ArrayList<Client>(clients));
	}

	@Override
	public Response<Client> save(Client client) {
		Client result = clientManager.save(client);
		return new Response<Client>(200, "ok", result);
	}

	@Override
	public Response<Client> getOne(Integer id) {
		Client result = clientManager.findOne(id);
		return new Response<Client>(200, "ok", result);
	}

	@Override
	public Response<Client> update(Integer id,Client client) {
		client.setId(id);
		Client result = clientManager.save(client);
		return new Response<Client>(200, "ok", result);
	}

	@Override
	public Response<Boolean> delete(Integer id) {
		Boolean result = clientManager.delete(id);
		return new Response<Boolean>(200, "ok", result);
	}

	@Override
	public ResponseList<Client> findByBirthday(Date birthday) {
		List<Client> clients =  clientManager.findAll();
		return new ResponseList<Client>(200, "ok", new ArrayList<Client>(clients));
	}

}
