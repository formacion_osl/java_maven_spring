package com.muigapps.curso2022.proyecto.ws;

import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.MethodParameter;
import org.springframework.data.domain.Page;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.server.ServerHttpRequest;
import org.springframework.http.server.ServerHttpResponse;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;
import org.springframework.web.servlet.mvc.method.annotation.ResponseBodyAdvice;

import com.muigapps.curso2022.proyecto.core.exception.CursoException;
import com.muigapps.curso2022.proyecto.core.filter.FilterClient;
import com.muigapps.curso2022.proyecto.core.manager.ClientManager;
import com.muigapps.curso2022.proyecto.core.model.Client;
import com.muigapps.curso2022.proyecto.ws.model.Response;
import com.muigapps.curso2022.proyecto.ws.model.ResponseList;

@RestController
@RequestMapping("/client")
public class ClientController implements ResponseBodyAdvice<Object>{
	
	@Autowired
	private ClientManager clientManager;
	
	
	@RequestMapping(path = {"/",""}, method = RequestMethod.POST, consumes = {"application/json", "application/xml"}, produces= {"application/json", "application/xml"})
	public Response<Client> save(@RequestBody Client client) throws CursoException{
		
		if(client.getId() != null) {
			throw new CursoException("Este metodo solo es para creación, por lo que el ID debe venir a nulo");
		}
		Client clientSave = clientManager.save(client);
		
		return new Response<Client>(200, "Ok", clientSave);
		
	}
	
	@RequestMapping(path = {"/filter/{page}/{size}","/filter/{page}/{size}/"}, method = RequestMethod.POST, consumes = {"application/json", "application/xml"}, produces= {"application/json", "application/xml"})
	public ResponseList<Client> filter(@PathVariable("page")Integer page, @PathVariable("size")Integer size, @RequestBody FilterClient filterClient) throws CursoException{
		
		Page<Client> pageClient = clientManager.filter(page,size,filterClient);
		
		return new ResponseList<Client>(200, "Ok", pageClient);
		
	}
	
	
	@RequestMapping(path = {"/{id}","/{id}/"}, method = RequestMethod.PUT, consumes = {"application/json", "application/xml"}, produces= {"application/json", "application/xml"})
	public Response<Client> update(@PathVariable("id") Integer id, @RequestBody Client client){
		client.setId(id);
		
		Client clientSave = clientManager.save(client);
		
		return new Response<Client>(200, "Ok", clientSave);
		
	}
	
	@RequestMapping(path = {"/all","/all/"}, method = RequestMethod.GET, produces= {"application/json", "application/xml"})
	public Response<List<Client>> getAll(){
		List<Client> clients = clientManager.findAll();
		
		return new Response<List<Client>>(200, "Ok", clients);
	}
	
	@RequestMapping(path = {"/page/{page}/{size}","/page/{page}/{size}/"}, method = RequestMethod.GET, produces= {"application/json", "application/xml"})
	public ResponseList<Client> getAll(@PathVariable("page")Integer page,@PathVariable("size")Integer size ){
		Page<Client> clients = clientManager.findAll(page,size);
		
		return new ResponseList<Client>(200, "Ok", clients);
	}
	
	@RequestMapping(path = {"/byname/{name}","/byname/{name}/"}, method = RequestMethod.GET, produces= {"application/json", "application/xml"})
	public Response<List<Client>> byname(@PathVariable("name") String name){
		List<Client> clients = clientManager.byname(name);
		
		return new Response<List<Client>>(200, "Ok", clients);
	}
	
	
	@RequestMapping(path = {"/one/{key}","/one/{key}/"}, method = RequestMethod.GET)
	public Response<Client> one(@PathVariable("key")Integer id){
		Client client = clientManager.findOne(id);
		
		return new Response<Client>(200, "Ok", client);
		
	}

	
	@RequestMapping(path = {"/one/{key}","/one/{key}/"}, method = RequestMethod.DELETE, produces= {"application/json", "application/xml"})
	public Response<Boolean> delete(@PathVariable("key")Integer id){
		Boolean result = clientManager.delete(id);
		
		return new Response<Boolean>(200, "Ok", result);
		
	}
	
	@RequestMapping(path = {"/findone","/findone/"}, method = RequestMethod.GET)
	public Response<Client> findone(@RequestParam(name = "id", required = false, defaultValue = "1") Integer id){
		Client client = clientManager.findOne(id);
		
		return new Response<Client>(200, "Ok", client);
		
	}
	

	@RequestMapping(path = {"/findonebyrequest","/findonebyrequest/"}, method = RequestMethod.GET)
	public Response<Client> findonebyrequest(HttpServletRequest request, HttpServletResponse response) throws Exception{
		try {
			Integer id = Integer.valueOf(request.getParameter("id"));
			
			response.setStatus(202);
			
			response.addHeader("Curso", "2022");
			
			
			Client client = clientManager.findOne(id);
			
			return new Response<Client>(200, "Ok", client);
		} catch (Exception e) {
			throw new Exception("Error al convertir a entero");
		}
		
	}
	

	
	@RequestMapping(path = {"/findbydate","/findbydate/"}, method = RequestMethod.GET)
	public Response<Client> findbydate(@RequestParam(name = "date") @DateTimeFormat(pattern = "yyyy-MM-dd") Date date){
		
		
		Client client = clientManager.findOne(1);
		
		return new Response<Client>(200, "Ok", client);
		
	}
	
	@RequestMapping(path = {"/findbydate/{date}","/findbydate/{date}/"}, method = RequestMethod.GET)
	public Response<Client> findbydateinurl(@PathVariable(name = "date") @DateTimeFormat(pattern = "yyyy-MM-dd") Date date){
		
		
		Client client = clientManager.findOne(1);
		
		return new Response<Client>(200, "Ok", client);
		
	}
	
	@ExceptionHandler(value = MethodArgumentTypeMismatchException.class)
	@ResponseStatus(value = HttpStatus.BAD_REQUEST)
	public Response<String> handleMethodArgumentTypeMismatchException(HttpServletRequest request, HttpServletResponse response){
		Response<String> result = new Response<String>(400, "KO", "El formato del parametro no es el esperado");
		
		return result;
		
	}
	
	@ExceptionHandler(value = HttpRequestMethodNotSupportedException.class)
	@ResponseStatus(value = HttpStatus.NOT_FOUND)
	public Response<String> handleHttpRequestMethodNotSupportedException(HttpServletRequest request, HttpServletResponse response){
		Response<String> result = new Response<String>(404, "KO", "El metodo no esta disponible o no existe");
		
		return result;
		
	}
	
	

	@Override
	public boolean supports(MethodParameter returnType, Class<? extends HttpMessageConverter<?>> converterType) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public Object beforeBodyWrite(Object body, MethodParameter returnType, MediaType selectedContentType,
			Class<? extends HttpMessageConverter<?>> selectedConverterType, ServerHttpRequest request,
			ServerHttpResponse response) {
		return body;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}
