package com.muigapps.curso2022.proyecto.config;

import javax.xml.ws.Endpoint;

import org.apache.cxf.Bus;
import org.apache.cxf.jaxws.EndpointImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.muigapps.curso2022.proyecto.ws.soap.AppointmentWSSoap;
import com.muigapps.curso2022.proyecto.ws.soap.ClientSoapWS;
import com.muigapps.curso2022.proyecto.ws.soap.MedicationSoapWS;
import com.muigapps.curso2022.proyecto.ws.soap.interceptor.SoapInterceptor;

@Configuration
public class CXFConfig {
	
	@Autowired
	private ClientSoapWS clientSoapWS;
	
	@Autowired
	private MedicationSoapWS medicationSoapWS;
	
	@Autowired
	private AppointmentWSSoap appointmentWSSoap;
	
	@Autowired
	private SoapInterceptor soapInterceptor;
	
	@Autowired
	private Bus bus;

	@Bean
	public Endpoint endPointClient() {
		EndpointImpl endpoint = new EndpointImpl(bus, clientSoapWS);
		endpoint.publish("/client");
		endpoint.getInInterceptors().add(soapInterceptor);
		return endpoint;
	}
	
	@Bean
	public Endpoint endPointMedication() {
		EndpointImpl endpoint = new EndpointImpl(bus, medicationSoapWS);
		endpoint.publish("/medication");
		return endpoint;
	}
	
	@Bean
	public Endpoint endPointAppointment() {
		EndpointImpl endpoint = new EndpointImpl(bus, appointmentWSSoap);
		endpoint.publish("/appointment");
		return endpoint;
	}
	
}
