package com.muigapps.curso2022.proyecto.config.security;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;

import com.muigapps.curso2022.proyecto.core.repository.UserRepository;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(securedEnabled = true, prePostEnabled = true)
public class SecurityConfig extends WebSecurityConfigurerAdapter {

	@Autowired
    private UserDetailsServiceImpl userDetailsService;
	@Autowired
	private UserRepository userRepository;
	
	public SecurityConfig() {
	}

	@Bean
	public BCryptPasswordEncoder bCryptPasswordEncoder() {
		return new BCryptPasswordEncoder();
	}

	@Override
	protected void configure(HttpSecurity httpSecurity) throws Exception {

		
		System.out.println("PASSWORD ------");
		
		System.out.println(bCryptPasswordEncoder().encode("12345678"));
		
		/*
		 * 1. Se desactiva el uso de cookies
		 * 2. Se activa la configuración CORS con los valores por defecto
		 * 3. Se desactiva el filtro CSRF
		 * 4. Se indica que el login no requiere autenticación
		 * 5. Se indica que el resto de URLs esten securizadas
		 */
		// /all /one/ / 
		httpSecurity
			.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS).and()
			.cors().and()
			.csrf().disable()
			.authorizeRequests().antMatchers(HttpMethod.POST, "/login").permitAll() //Indicamos que a la URL de login pueden acceder cualquier persona
			.antMatchers(HttpMethod.GET, "**").hasAnyAuthority("READER","ADMIN") //indicamos que a los metodos GET puede acceder cualquier persona con el permiso de READER o de ADMIN
			.antMatchers(HttpMethod.POST, "/client/filter/**").hasAnyAuthority("READER","ADMIN") //Indicamos que a la url de /client/filter/** puede acceder cualquier persona con el permiso de READER o de ADMIN
			.antMatchers(HttpMethod.POST, "**").hasAnyAuthority("ADMIN") // A los metodos POST solo se puede acceder con el permiso de ADMIN
			.antMatchers(HttpMethod.PUT, "**").hasAnyAuthority("ADMIN")// A los metodos PUT solo se puede acceder con el permiso de ADMIN
			.antMatchers(HttpMethod.DELETE, "**").hasAnyAuthority("ADMIN")// A los metodos DELETE solo se puede acceder con el permiso de ADMIN
			.anyRequest().authenticated().and() //cualquier otra URL es necesario estar autenticado
			.addFilter(new JWTAuthenticationFilter(authenticationManager(),userRepository))  //Indicamos cual es el filtro de autenticación
			.addFilter(new JWTAuthorizationFilter(authenticationManager(),userRepository)); //Indicamos cual es el filtro de autorización
	}


	
	@Override
	public void configure(AuthenticationManagerBuilder auth) throws Exception {
		// Se define la clase que recupera los usuarios y el algoritmo para procesar las passwords
		auth.userDetailsService(userDetailsService).passwordEncoder(bCryptPasswordEncoder());
	}

	@Bean
	public CorsConfigurationSource corsConfigurationSource() {
		List<String> methods = Collections.unmodifiableList(Arrays.asList(HttpMethod.GET.name(), HttpMethod.HEAD.name(), HttpMethod.POST.name(),
				HttpMethod.PUT.name(), HttpMethod.DELETE.name()));
		final UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
		CorsConfiguration corsConfiguration = new CorsConfiguration();
		corsConfiguration.setAllowedMethods(methods);
		List<String> headers = Collections.unmodifiableList(Arrays.asList(HttpHeaders.ACCEPT, HttpHeaders.CONTENT_TYPE, HttpHeaders.CONTENT_LENGTH,
				HttpHeaders.CONTENT_LANGUAGE, HttpHeaders.ACCESS_CONTROL_ALLOW_HEADERS, HttpHeaders.ACCEPT_ENCODING, HttpHeaders.ACCEPT_LANGUAGE,
				HttpHeaders.CONTENT_ENCODING, HttpHeaders.CONTENT_LANGUAGE, HttpHeaders.AUTHORIZATION, HttpHeaders.ALLOW, "refreshtoken"));
		corsConfiguration.setExposedHeaders(headers);
		source.registerCorsConfiguration("/**", corsConfiguration.applyPermitDefaultValues());
		return source;
	}
	
}

