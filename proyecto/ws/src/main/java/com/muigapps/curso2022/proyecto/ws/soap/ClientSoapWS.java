package com.muigapps.curso2022.proyecto.ws.soap;

import java.util.Date;

import javax.jws.WebService;

import com.muigapps.curso2022.proyecto.core.model.Client;
import com.muigapps.curso2022.proyecto.ws.model.Response;
import com.muigapps.curso2022.proyecto.ws.model.ResponseList;

@WebService
public interface ClientSoapWS {
	
	public ResponseList<Client> findAll();
	
	public Response<Client> save(Client client);
	
	public Response<Client> getOne(Integer id);
	
	public Response<Client> update(Integer id,Client client);
	
	public Response<Boolean> delete(Integer id);
	
	public ResponseList<Client> findByBirthday(Date birthday);

}
