package com.muigapps.curso2022.proyecto.ws.model;

import java.io.Serializable;
import java.util.ArrayList;

import javax.xml.bind.annotation.XmlRootElement;

import org.springframework.data.domain.Page;

@XmlRootElement(name = "response")
public class ResponseList<T> implements Serializable {

	private static final long serialVersionUID = 4963741287617801800L;

	private Integer code;
	private String message;
	private ArrayList<T> data;
	private Long totalElement;
	private Long elementsInpage;
	private Integer page;
	private Integer totalPages;
	
	
	public Integer getCode() {
		return code;
	}
	public void setCode(Integer code) {
		this.code = code;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public ArrayList<T> getData() {
		return data;
	}
	public void setData(ArrayList<T> data) {
		this.data = data;
	}
	public Long getTotalElement() {
		return totalElement;
	}
	public void setTotalElement(Long totalElement) {
		this.totalElement = totalElement;
	}
	public Long getElementsInpage() {
		return elementsInpage;
	}
	public void setElementsInpage(Long elementsInpage) {
		this.elementsInpage = elementsInpage;
	}
	public Integer getPage() {
		return page;
	}
	public void setPage(Integer page) {
		this.page = page;
	}
	public Integer getTotalPages() {
		return totalPages;
	}
	public void setTotalPages(Integer totalPages) {
		this.totalPages = totalPages;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	public ResponseList() {
		super();
	}
	public ResponseList(Integer code, String message, ArrayList<T> data) {
		super();
		this.code = code;
		this.message = message;
		this.data = data;
	}
	public ResponseList(Integer code, String message, Page<T> data) {
		super();
		this.code = code;
		this.message = message;
		this.data = new ArrayList(data.getContent());
		this.totalElement = data.getTotalElements();
		this.totalPages = data.getTotalPages();
		this.page = data.getNumber();
		this.elementsInpage = Long.valueOf(data.getSize());
	}
	
	
	
	
	
	
	
	
	
}
