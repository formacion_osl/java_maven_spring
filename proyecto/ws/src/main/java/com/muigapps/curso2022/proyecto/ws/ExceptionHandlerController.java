package com.muigapps.curso2022.proyecto.ws;

import javax.persistence.EntityNotFoundException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.MethodParameter;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.HttpMessageNotWritableException;
import org.springframework.http.server.ServerHttpRequest;
import org.springframework.http.server.ServerHttpResponse;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;
import org.springframework.web.server.MethodNotAllowedException;
import org.springframework.web.servlet.NoHandlerFoundException;
import org.springframework.web.servlet.mvc.method.annotation.ResponseBodyAdvice;

import com.muigapps.curso2022.proyecto.core.exception.CursoException;
import com.muigapps.curso2022.proyecto.ws.model.Response;

@RestControllerAdvice
public class ExceptionHandlerController implements ResponseBodyAdvice<Object>{

	@Value("${cxf.path}")
	private String soapUrl;
	

	@ExceptionHandler(value = MethodArgumentTypeMismatchException.class)
	@ResponseStatus(value = HttpStatus.BAD_REQUEST)
	public Response<String> handleMethodArgumentTypeMismatchException(HttpServletRequest request, HttpServletResponse response){
		Response<String> result = new Response<String>(400, "KO", "El formato del parametro no es el esperado");
		
		return result;
		
	}
	
	@ExceptionHandler(value = NoHandlerFoundException.class)
	@ResponseStatus(value = HttpStatus.NOT_FOUND)
	public Response<String> handleHttpRequestMethodNotSupportedException(HttpServletRequest request, HttpServletResponse response){
		if(request.getRequestURI().indexOf(soapUrl) >= 0) {
			Response<String> result = new Response<String>(404, "KO", "SOAP no esta habilitado");
			return result;
		} else {
			Response<String> result = new Response<String>(404, "KO", "El metodo no esta disponible o no existe");
			return result;
		}
	}
	
	@ExceptionHandler(value = MethodNotAllowedException.class)
	@ResponseStatus(value = HttpStatus.NOT_FOUND)
	public Response<String> handleHttpRequestMethodNotAllowedException(HttpServletRequest request, HttpServletResponse response){
		Response<String> result = new Response<String>(404, "KO", "El metodo no esta disponible o no existe");
		
		return result;
		
	}

	@ExceptionHandler(value = HttpRequestMethodNotSupportedException.class)
	@ResponseStatus(value = HttpStatus.NOT_FOUND)
	public Response<String> handleHttpRequestHttpRequestMethodNotSupportedException(HttpServletRequest request, HttpServletResponse response){
		Response<String> result = new Response<String>(404, "KO", "El metodo no esta disponible o no existe");
		
		return result;
		
	}

	@ExceptionHandler(value = CursoException.class)
	@ResponseStatus(value = HttpStatus.BAD_REQUEST)
	public Response<String> handleCursoException(HttpServletRequest request, HttpServletResponse response, CursoException e){
		Response<String> result = new Response<String>(400, "KO", e.getMessage());
		
		return result;
		
	}
	
	@ExceptionHandler(value = HttpMessageNotWritableException.class)
	@ResponseStatus(value = HttpStatus.NOT_FOUND)
	public Response<String> handleHttpMessageNotWritableException(HttpServletRequest request, HttpServletResponse response){
		Response<String> result = new Response<String>(404, "KO", "El objeto al que accedemos no esta disponible");
		
		return result;
		
	}
	

	@ExceptionHandler(value = EntityNotFoundException.class)
	@ResponseStatus(value = HttpStatus.NOT_FOUND)
	public Response<String> handleEntityNotFoundException(HttpServletRequest request, HttpServletResponse response){
		Response<String> result = new Response<String>(404, "KO", "El objeto al que accedemos no esta disponible");
		
		return result;
		
	}
	

	@Override
	public boolean supports(MethodParameter returnType, Class<? extends HttpMessageConverter<?>> converterType) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public Object beforeBodyWrite(Object body, MethodParameter returnType, MediaType selectedContentType,
			Class<? extends HttpMessageConverter<?>> selectedConverterType, ServerHttpRequest request,
			ServerHttpResponse response) {
		return body;
	}

}
