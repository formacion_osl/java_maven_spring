package com.muigapps.curso2022.proyecto.config.security;

import java.util.HashSet;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.muigapps.curso2022.proyecto.core.repository.UserRepository;


@Service
public class UserDetailsServiceImpl implements UserDetailsService{

	@Autowired
	private UserRepository userRepository;
	
	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		com.muigapps.curso2022.proyecto.core.model.User user = userRepository.findOneByEmail(username);
		if (user == null) {
            throw new UsernameNotFoundException(username);
        }
		return new User(user.getEmail(), user.getPass(), getPermissions(user));
	}
	
	private Set<GrantedAuthority> getPermissions(com.muigapps.curso2022.proyecto.core.model.User user){
		Set<GrantedAuthority> permissions = new HashSet<>();
		
		if(user.getRol() != null && user.getRol().getCode() != null) {
			permissions.add(new SimpleGrantedAuthority(user.getRol().getCode()));
		}
		
		return permissions;
	}

}
