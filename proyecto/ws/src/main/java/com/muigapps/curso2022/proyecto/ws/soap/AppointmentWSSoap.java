package com.muigapps.curso2022.proyecto.ws.soap;

import javax.jws.WebService;

import com.muigapps.curso2022.proyecto.core.model.Appointment;
import com.muigapps.curso2022.proyecto.ws.model.Response;
import com.muigapps.curso2022.proyecto.ws.model.ResponseList;

@WebService
public interface AppointmentWSSoap {

	public ResponseList<Appointment> findAll();
	
	public Response<Appointment> save(Appointment appointment);
	
	public Response<Appointment> getOne(Integer id);
	
	public Response<Appointment> update(Integer id,Appointment appointment);
	
	public Response<Boolean> delete(Integer id);
	
}
