package com.muigapps.curso2022.proyecto.ws.model;

import java.io.Serializable;

public class Response<T> implements Serializable {

	private static final long serialVersionUID = 4963741287617801800L;

	private Integer code;
	private String message;
	private T data;
	
	
	public Integer getCode() {
		return code;
	}
	public void setCode(Integer code) {
		this.code = code;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public T getData() {
		return data;
	}
	public void setData(T data) {
		this.data = data;
	}
	public Response() {
		super();
	}
	public Response(Integer code, String message, T data) {
		super();
		this.code = code;
		this.message = message;
		this.data = data;
	}
	
	
	
	
	
	
}
