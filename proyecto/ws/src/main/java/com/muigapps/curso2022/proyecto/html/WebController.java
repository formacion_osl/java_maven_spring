package com.muigapps.curso2022.proyecto.html;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.muigapps.curso2022.proyecto.core.manager.ClientManager;
import com.muigapps.curso2022.proyecto.core.model.Client;

@Controller
@RequestMapping("/web")
public class WebController {
	
	@Autowired
	private ClientManager clientManager;

	@RequestMapping(path = {"/test","/test/"})
	public String test(HttpServletRequest request, Model model) {
		
		model.addAttribute("titulo", "Hola mundo como atributo");
		
		return "test";
	}
	
	@RequestMapping(path = {"/clients","/clients/"})
	public String clients(HttpServletRequest request, Model model) {
		
		List<Client> clients = clientManager.findAll();
		
		model.addAttribute("titulo", "Lista de clientes");
		model.addAttribute("clients", clients);
		
		return "listclients";
	}
}
