package com.muigapps.curso2022.proyecto.ws.soap.interceptor;

import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import org.apache.cxf.binding.soap.SoapMessage;
import org.apache.cxf.binding.soap.interceptor.AbstractSoapInterceptor;
import org.apache.cxf.interceptor.Fault;
import org.apache.cxf.phase.Phase;
import org.apache.cxf.transport.http.AbstractHTTPDestination;
import org.springframework.stereotype.Component;

@Component
public class SoapInterceptor extends AbstractSoapInterceptor{

	public SoapInterceptor() {
		super(Phase.RECEIVE);
	}

	@Override
	public void handleMessage(SoapMessage message) throws Fault {
		 HttpServletRequest request  = (HttpServletRequest) message.get(AbstractHTTPDestination.HTTP_REQUEST);
		
		 String url = request.getRequestURI();
		 
		 if(request.getQueryString() != null && !request.getQueryString().isEmpty()) {
			 url = url + "?" + request.getQueryString();
		 }
		 
	     SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
	  
	     System.out.println("Nueva llamada SOAP. URL: " + url + " ==>  A LAS : " + format.format(new Date()));
	}

}
