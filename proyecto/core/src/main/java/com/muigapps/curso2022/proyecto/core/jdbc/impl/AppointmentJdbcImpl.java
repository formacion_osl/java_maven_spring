package com.muigapps.curso2022.proyecto.core.jdbc.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.muigapps.curso2022.proyecto.core.jdbc.AppointmentJdbc;
import com.muigapps.curso2022.proyecto.core.jdbc.ClientJdbc;
import com.muigapps.curso2022.proyecto.core.jdbc.mapper.AppointmentMapper;
import com.muigapps.curso2022.proyecto.core.model.Appointment;
import com.muigapps.curso2022.proyecto.core.model.Client;

@Repository
public class AppointmentJdbcImpl implements AppointmentJdbc{
	
	@Value("${appointment.find.all}")
	private String sqlFindAll;
	@Value("${appointment.find.one}")
	private String sqlFindOne;
	@Value("${appointment.find.maxid}")
	private String sqlFindMaxID;
	@Value("${appointment.save}")
	private String sqlSave;
	@Value("${appointment.update}")
	private String sqlUpdate;
	@Value("${appointment.delete}")
	private String sqlDelete;
	
	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	@Autowired
	@Qualifier("clientJDBC")
	private ClientJdbc clientJdbc;

	
	@Override
	public Appointment findOne(Integer id) {
		return jdbcTemplate.queryForObject(sqlFindOne, new AppointmentMapper(), id);
	}

	@Override
	public Appointment save(Appointment appointment) {
		Integer id = null;
		
		if(appointment.getId() == null) {
			Object[] args = new Object[3];
			args[0] = appointment.getName();
			args[1] = appointment.getDate();
			args[2] = getClient(appointment.getClient());
			
			jdbcTemplate.update(sqlSave, args);
			
			id = jdbcTemplate.queryForObject(sqlFindMaxID, Integer.class);
			
		} else {
			Object[] args = new Object[4];
			args[0] = appointment.getName();
			args[1] = appointment.getDate();
			args[2] = appointment.getClient() != null ? appointment.getClient().getId(): null;
			args[3] = getClient(appointment.getClient());
			
			jdbcTemplate.update(sqlUpdate, args);
			
			id = appointment.getId();
			
		}
		return findOne(id);
	}
	
	private Integer getClient(Client client) {
		Integer id = client.getId();
		
		if(id == null) {
			client = clientJdbc.save(client);
			id = client.getId();
		}
		
		return id;
		
	}

	@Override
	public List<Appointment> findAll() {
		return jdbcTemplate.query(sqlFindAll, new AppointmentMapper());
	}

	@Override
	public Boolean delete(Integer id) {
		jdbcTemplate.update(sqlDelete, id);
		return true;
	}
	
	
	

}
