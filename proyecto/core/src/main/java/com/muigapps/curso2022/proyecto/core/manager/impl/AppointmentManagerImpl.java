package com.muigapps.curso2022.proyecto.core.manager.impl;

import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Component;

import com.muigapps.curso2022.proyecto.core.jdbc.AppointmentJdbc;
import com.muigapps.curso2022.proyecto.core.manager.AppointmentManager;
import com.muigapps.curso2022.proyecto.core.model.Appointment;
import com.muigapps.curso2022.proyecto.core.model.Client;
import com.muigapps.curso2022.proyecto.core.repository.AppointmentRepository;

@Component
public class AppointmentManagerImpl implements AppointmentManager{

	@Autowired
	private AppointmentRepository repository;
	

	@Autowired
	private AppointmentJdbc appointmentJdbc;
	
	
	@Override
	public Appointment findOne(Integer id) {
		return repository.findById(id).orElse(null);
	}

	@Transactional
	@Override
	public Appointment save(Appointment appointment) {
		return repository.save(appointment);
	}

	@Override
	public List<Appointment> findAll() {
		return repository.findAll();
	}

	@Override
	public Boolean delete(Integer id) {
		repository.deleteById(id);
		return true;
	}
	
	@Override
	public Page<Appointment> findAll(Integer page, Integer size) {
		return repository.findAll(PageRequest.of(page, size, Sort.by(Direction.DESC, "id")));
	}

	@Override
	public List<Appointment> findBeforeDateAndNameClient(Date date, String nameClient) {
		return repository.findBeforeDateAndNameClient(date, "%"+nameClient+"%");
	}

}
