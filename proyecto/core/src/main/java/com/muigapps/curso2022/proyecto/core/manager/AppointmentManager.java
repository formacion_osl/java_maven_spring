package com.muigapps.curso2022.proyecto.core.manager;

import java.util.Date;
import java.util.List;

import org.springframework.data.domain.Page;

import com.muigapps.curso2022.proyecto.core.model.Appointment;

public interface AppointmentManager {
	
	Appointment findOne(Integer id);
	
	Appointment save(Appointment appointment);
	
	List<Appointment> findAll();
	
	Boolean delete(Integer id);

	Page<Appointment> findAll(Integer page, Integer size);
	
	List<Appointment> findBeforeDateAndNameClient(Date date, String nameClient);

}
