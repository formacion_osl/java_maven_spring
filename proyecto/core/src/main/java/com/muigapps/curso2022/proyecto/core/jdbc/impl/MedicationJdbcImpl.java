package com.muigapps.curso2022.proyecto.core.jdbc.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.muigapps.curso2022.proyecto.core.jdbc.MedicationJdbc;
import com.muigapps.curso2022.proyecto.core.jdbc.mapper.MedicationMapper;
import com.muigapps.curso2022.proyecto.core.model.Medication;

@Repository
public class MedicationJdbcImpl implements MedicationJdbc{
	
	@Value("${medication.find.all}")
	private String sqlFindAll;
	@Value("${medication.find.one}")
	private String sqlFindOne;
	@Value("${medication.find.maxid}")
	private String sqlFindMaxID;
	@Value("${medication.save}")
	private String sqlSave;
	@Value("${medication.update}")
	private String sqlUpdate;
	@Value("${medication.delete}")
	private String sqlDelete;
	
	@Autowired
	private JdbcTemplate jdbcTemplate;

	
	@Override
	public Medication findOne(Integer id) {
		return jdbcTemplate.queryForObject(sqlFindOne, new MedicationMapper(), id);
	}

	@Override
	public Medication save(Medication medication) {
		Integer id = null;
		
		if(medication.getId() == null) {
			Object[] args = new Object[1];
			args[0] = medication.getName();
			
			jdbcTemplate.update(sqlSave, args);
			
			id = jdbcTemplate.queryForObject(sqlFindMaxID, Integer.class);
			
		} else {
			Object[] args = new Object[2];
			args[0] = medication.getName();
			args[1] = medication.getId();
			
			jdbcTemplate.update(sqlUpdate, args);
			
			id = medication.getId();
			
		}
		return findOne(id);
	}

	@Override
	public List<Medication> findAll() {
		return jdbcTemplate.query(sqlFindAll, new MedicationMapper());
	}

	@Override
	public Boolean delete(Integer id) {
		jdbcTemplate.update(sqlDelete, id);
		return true;
	}
	
	
	

}
