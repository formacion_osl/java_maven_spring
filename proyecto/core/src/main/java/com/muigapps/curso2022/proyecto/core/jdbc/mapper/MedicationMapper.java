package com.muigapps.curso2022.proyecto.core.jdbc.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.muigapps.curso2022.proyecto.core.model.Medication;

public class MedicationMapper implements RowMapper<Medication>{

	
	@Override
	public Medication mapRow(ResultSet rs, int rowNum) throws SQLException {
		Medication medication = new Medication();
		medication.setId(rs.getInt("id"));
		medication.setName(rs.getString("name"));
		return medication;
	}

}
