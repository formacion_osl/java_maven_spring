package com.muigapps.curso2022.proyecto.core.model;

import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.muigapps.curso2022.proyecto.core.model.serializer.AppintmentSeriealizer;

@Entity
@Table(name = "client")
@XmlRootElement
public class Client {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;
	private String name;
	private String lastname = "Apellido por defecto";
	private String vat;
	private Date birthday;
	@Transient
	private String nameComplete;
	
	@JsonSerialize(using = AppintmentSeriealizer.class)
	@OneToMany(mappedBy = "client")
	private List<Appointment> appointments;
	
	
	@OneToOne(mappedBy = "client")
	private Street street;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getLastname() {
		return lastname;
	}
	public void setLastname(String lastname) {
		if(lastname == null  || lastname.isEmpty()) {
			this.lastname = "Apellido por defecto";
		} else {
			this.lastname = lastname;
		}
	}
	public String getVat() {
		return vat;
	}
	public void setVat(String vat) {
		this.vat = vat;
	}
	public Date getBirthday() {
		return birthday;
	}
	public void setBirthday(Date birthday) {
		this.birthday = birthday;
	}
	public String getNameComplete() {
		return name + " " + lastname;
	}
	public void setNameComplete(String nameComplete) {
		this.nameComplete = nameComplete;
	}

	@XmlElement(name = "appointment")
	public List<Appointment> getAppointments() {
		return appointments;
	}
	public void setAppointments(List<Appointment> appointments) {
		this.appointments = appointments;
	}
	public Street getStreet() {
		return street;
	}
	public void setStreet(Street street) {
		this.street = street;
	}
	
	
	
	public Client() {
		super();
	}
	
	public Client(Integer id, String name, String lastname, String vat, Date birthday, Street street) {
		super();
		this.id = id;
		this.name = name;
		this.lastname = lastname;
		this.vat = vat;
		this.birthday = birthday;
		this.street = street;
	}
	
	
	
	
	
	
	
	
	
}
