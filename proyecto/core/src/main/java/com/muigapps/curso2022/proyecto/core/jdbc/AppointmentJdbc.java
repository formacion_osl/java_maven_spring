package com.muigapps.curso2022.proyecto.core.jdbc;

import java.util.List;

import com.muigapps.curso2022.proyecto.core.model.Appointment;

public interface AppointmentJdbc {

	Appointment findOne(Integer id);
	
	Appointment save(Appointment medication);
	
	List<Appointment> findAll();
	
	Boolean delete(Integer id);
	
}
