package com.muigapps.curso2022.proyecto.core.repository;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.muigapps.curso2022.proyecto.core.model.User;

public interface UserRepository extends JpaRepository<User, Long>{
	
	@Query("select u from User u where u.email = :email")
	User findOneByEmail(@Param("email") String email);


}
