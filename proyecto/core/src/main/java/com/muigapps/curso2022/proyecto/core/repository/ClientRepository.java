package com.muigapps.curso2022.proyecto.core.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.muigapps.curso2022.proyecto.core.model.Client;

public interface ClientRepository extends JpaRepository<Client, Integer>, JpaSpecificationExecutor<Client>, ClientExteRepository{

	
	@Query("select c from Client c where c.name like :name ")
	List<Client> findWithName(@Param("name")String name);
	
	@Query(value = "select c.* from Client c where c.name like :name ", nativeQuery = true)
	List<Client> findWithNameNative(@Param("name")String name);
	
	List<Client> findByNameContaining(String name);
	
	
	/** Buscamos por nombre y apellido  == */
	List<Client> findByNameAndLastnameOrderByIdAsc(String name, String lastname);
	
	/** Buscamos por nombre y apellido  Like */
	List<Client> findByNameLikeAndLastnameLike(String name, String lastname);
}
