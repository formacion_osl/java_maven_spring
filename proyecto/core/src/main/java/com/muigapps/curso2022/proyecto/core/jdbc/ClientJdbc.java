package com.muigapps.curso2022.proyecto.core.jdbc;

import java.util.List;

import com.muigapps.curso2022.proyecto.core.model.Client;

public interface ClientJdbc {

	Client findOne(Integer id);
	
	Client save(Client client);
	
	List<Client> findAll();
	
	Boolean delete(Integer id);
}
