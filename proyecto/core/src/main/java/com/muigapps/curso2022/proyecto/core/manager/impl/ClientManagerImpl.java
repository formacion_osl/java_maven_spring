package com.muigapps.curso2022.proyecto.core.manager.impl;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Component;

import com.muigapps.curso2022.proyecto.core.filter.FilterClient;
import com.muigapps.curso2022.proyecto.core.jdbc.ClientJdbc;
import com.muigapps.curso2022.proyecto.core.manager.ClientManager;
import com.muigapps.curso2022.proyecto.core.model.Client;
import com.muigapps.curso2022.proyecto.core.repository.ClientRepository;
import com.muigapps.curso2022.proyecto.core.repository.specifications.ClientSpecifications;

@Component
public class ClientManagerImpl implements ClientManager{
	
	@Autowired
	private ClientRepository clientRepository;
	
	@Autowired
	@Qualifier(value = "clientDao")
	private ClientJdbc clientJdbc;

	@Override
	public Client findOne(Integer id) {
		return clientRepository.findById(id).orElse(null);
	}

	@Transactional
	@Override
	public Client save(Client client) {
		return clientRepository.save(client);
	}

	@Override
	public List<Client> findAll() {
		// TODO Auto-generated method stub
		return clientRepository.findAll(Sort.by(Direction.ASC, "id"));
	}

	@Override
	public Boolean delete(Integer id) {
		clientRepository.deleteById(id);
		return true;
	}

	@Override
	public Page<Client> findAll(Integer page, Integer size) {
		return clientRepository.findAll(PageRequest.of(page, size, Sort.by(Direction.DESC, "id")));
	}

	@Override
	public List<Client> byname(String name) {
		
		return clientRepository.findWithName("%"+name+"%");
	}

	
	@Override
	public Page<Client> filter(Integer page, Integer size, FilterClient filterClient) {
		return clientRepository.findAll(ClientSpecifications.getInstance(filterClient), PageRequest.of(page, size));
	}

}
