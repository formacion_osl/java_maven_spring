package com.muigapps.curso2022.proyecto.core.jdbc.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.muigapps.curso2022.proyecto.core.jdbc.ClientJdbc;
import com.muigapps.curso2022.proyecto.core.jdbc.mapper.ClientMapper;
import com.muigapps.curso2022.proyecto.core.model.Client;

@Repository("clientJDBC")
public class ClientJdbcImpl implements ClientJdbc{
	
	@Value("${client.find.all}")
	private String sqlFindAll;
	@Value("${client.find.one}")
	private String sqlFindOne;
	@Value("${client.find.maxid}")
	private String sqlFindMaxID;
	@Value("${client.save}")
	private String sqlSave;
	@Value("${client.update}")
	private String sqlUpdate;
	@Value("${client.delete}")
	private String sqlDelete;
	

	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	public JdbcTemplate getJdbcTemplate() {
		return jdbcTemplate;
	}

	public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;
	}

	@Override
	public Client findOne(Integer id) {
		return jdbcTemplate.queryForObject(sqlFindOne, new ClientMapper(), id);
	}

	@Override
	public Client save(Client client) {
		Integer id = null;
		
		if(client.getId() == null) {
			
			int i = 0;
			Object[] args = new Object[4];
			args[i++] = client.getName();
			args[i++] = client.getLastname();
			args[i++] = client.getVat();
			args[i++] = client.getBirthday();
			
			jdbcTemplate.update(sqlSave, args);
			
			id = jdbcTemplate.queryForObject(sqlFindMaxID, Integer.class);
			
			
					
		} else {
			
			int i = 0;
			Object[] args = new Object[5];
			args[i++] = client.getName();
			args[i++] = client.getLastname();
			args[i++] = client.getVat();
			args[i++] = client.getBirthday();
			args[i++] = client.getId();
			
			jdbcTemplate.update(sqlUpdate, args);
			
			id = client.getId();
		}
		
		return findOne(id);
	}

	@Override
	public List<Client> findAll() {
		return jdbcTemplate.query(sqlFindAll, new ClientMapper());
	}

	@Override
	public Boolean delete(Integer id) {
		
		jdbcTemplate.update(sqlDelete,id);
		
		return true;
	}
	
	

}
