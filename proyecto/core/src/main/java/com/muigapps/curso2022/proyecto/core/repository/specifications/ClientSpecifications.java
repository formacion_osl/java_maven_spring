package com.muigapps.curso2022.proyecto.core.repository.specifications;

import java.util.ArrayList;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.data.jpa.domain.Specification;

import com.muigapps.curso2022.proyecto.core.filter.FilterClient;
import com.muigapps.curso2022.proyecto.core.model.Client;

public class ClientSpecifications implements Specification<Client>{

	private static final long serialVersionUID = 8322840279099484796L;

	private FilterClient filter;
	
	private ClientSpecifications instance;
	
	public static ClientSpecifications getInstance(FilterClient filter) {
		return new ClientSpecifications(filter);
	}

	
	public ClientSpecifications(FilterClient filter) {
		super();
		this.filter = filter;
	}



	@Override
	public Predicate toPredicate(Root<Client> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
		
		query.distinct(false);
		
		ArrayList<Predicate> predicates = new ArrayList<Predicate>();
		
		if (filter != null && filter.getName() != null && !filter.getName().isEmpty()) {
			Predicate predicate = criteriaBuilder.like(criteriaBuilder.lower(root.get("name")), "%"+filter.getName().toLowerCase()+"%");
			predicates.add(predicate);
		}
		if(filter != null && filter.getLastname() != null && !filter.getLastname().isEmpty()) {
			Predicate predicate = criteriaBuilder.like(criteriaBuilder.lower(root.get("lastname")), "%"+filter.getLastname().toLowerCase()+"%");
			predicates.add(predicate);
		}
		if(filter != null && filter.getVat() != null && !filter.getVat().isEmpty()) {
			Predicate predicate = criteriaBuilder.like(criteriaBuilder.lower(root.get("vat")), "%"+filter.getVat().toLowerCase()+"%");
			predicates.add(predicate);
		}
		if(filter != null && filter.getFromDate() != null && filter.getToDate() != null) {
			Predicate predicate =  criteriaBuilder.between(root.get("birthday"), filter.getFromDate(), filter.getToDate());
			predicates.add(predicate);
		} else if(filter != null && filter.getFromDate() != null) {
			Predicate predicate = criteriaBuilder.greaterThanOrEqualTo(root.get("birthday"), filter.getFromDate());
			predicates.add(predicate);
		} else if(filter != null && filter.getToDate() != null) {
			Predicate predicate = criteriaBuilder.lessThanOrEqualTo(root.get("birthday"), filter.getToDate());
			predicates.add(predicate);
		}
		
		if((filter != null && filter.getCity() != null && !filter.getCity().isEmpty()) || (filter != null && filter.getPostalCode() != null && !filter.getPostalCode().isEmpty())) {
			Join street = root.join("street",JoinType.LEFT);
			
			if(filter != null && filter.getCity() != null && !filter.getCity().isEmpty()) {
				// 1º Forma
				/*Predicate predicate =  criteriaBuilder.equal(criteriaBuilder.lower(root.get("street").get("city")), filter.getCity().toLowerCase());
				predicates.add(predicate);*/
				
				//2º Forma
				Predicate predicate =  criteriaBuilder.equal(criteriaBuilder.lower(street.get("city")), filter.getCity().toLowerCase());
				predicates.add(predicate);
			}
			
			if(filter != null && filter.getPostalCode() != null && !filter.getPostalCode().isEmpty()) {
				// 1º Forma
				Predicate predicate =  criteriaBuilder.equal(criteriaBuilder.lower(street.get("postalCode")), filter.getPostalCode().toLowerCase());
				predicates.add(predicate);
			}
		}
		
		if(filter != null && (filter.getAppointmentFom() != null || filter.getAppointmentTo() != null)) {

			Join appointments = root.join("appointments");
			
			if(filter != null && filter.getAppointmentFom() != null && filter.getAppointmentTo() != null) {
				Predicate predicate =  criteriaBuilder.between(appointments.get("date"), filter.getAppointmentFom(), filter.getAppointmentTo());
				predicates.add(predicate);
			} else if(filter != null && filter.getAppointmentFom() != null) {
				Predicate predicate = criteriaBuilder.greaterThanOrEqualTo(appointments.get("date"), filter.getAppointmentFom());
				predicates.add(predicate);
			} else if(filter != null && filter.getAppointmentTo() != null) {
				Predicate predicate = criteriaBuilder.lessThanOrEqualTo(appointments.get("date"), filter.getAppointmentTo());
				predicates.add(predicate);
			}
		}
		
		
		
		if(predicates.isEmpty()) {
			return null;
		} else {
			Predicate[] arrayPredicates = new Predicate[predicates.size()];
			int i = 0;
			for(Predicate p: predicates) {
				arrayPredicates[i++] = p;
			}
			
			return criteriaBuilder.and(arrayPredicates);
		}
	}

}
