package com.muigapps.curso2022.proyecto.core.jdbc.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.muigapps.curso2022.proyecto.core.model.Client;

public class ClientMapper implements RowMapper<Client>{

	
	@Override
	public Client mapRow(ResultSet rs, int rowNum) throws SQLException {
		Client client = new Client();
		client.setId(rs.getInt("id"));
		client.setName(rs.getString("name"));
		client.setLastname(rs.getString("lastname"));
		client.setVat(rs.getString("vat"));
		client.setBirthday(rs.getDate("birthday"));
		
		return client;
	}
	
}
