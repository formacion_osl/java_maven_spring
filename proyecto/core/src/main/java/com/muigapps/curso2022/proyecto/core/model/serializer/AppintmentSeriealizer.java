package com.muigapps.curso2022.proyecto.core.model.serializer;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.List;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;
import com.muigapps.curso2022.proyecto.core.model.Appointment;

public class AppintmentSeriealizer extends StdSerializer<List<Appointment>>{


	private static final long serialVersionUID = 1L;
	

	public AppintmentSeriealizer() {
        this(null);
    }
  
    public AppintmentSeriealizer(Class<List<Appointment>> t) {
        super(t);
    }
    

	@Override
	public void serialize(List<Appointment> value, JsonGenerator gen, SerializerProvider provider)
			throws IOException {
		if(value != null && value.size() > 0) {
			gen.writeStartArray();
			
			for(Appointment appointment: value) {
				gen.writeStartObject();
				gen.writeStringField("name", appointment.getName());
				SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
				gen.writeStringField("date", format.format(appointment.getDate()));
				gen.writeEndObject();
			}
			
			gen.writeEndArray();
		} else {
			gen.writeNull();
		}
		
	}

}
