package com.muigapps.curso2022.proyecto.core.jdbc;

import java.util.List;

import com.muigapps.curso2022.proyecto.core.model.Medication;

public interface MedicationJdbc {

	Medication findOne(Integer id);
	
	Medication save(Medication medication);
	
	List<Medication> findAll();
	
	Boolean delete(Integer id);
	
}
