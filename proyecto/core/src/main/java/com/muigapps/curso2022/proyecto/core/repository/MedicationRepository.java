package com.muigapps.curso2022.proyecto.core.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.muigapps.curso2022.proyecto.core.model.Client;
import com.muigapps.curso2022.proyecto.core.model.Medication;

public interface MedicationRepository extends JpaRepository<Medication, Integer>{

}
