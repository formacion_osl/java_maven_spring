package com.muigapps.curso2022.proyecto.core.repository;

import java.util.List;

import com.muigapps.curso2022.proyecto.core.model.Client;

public interface ClientExteRepository {
	
	public List<Client> findBYName(String name);

}
