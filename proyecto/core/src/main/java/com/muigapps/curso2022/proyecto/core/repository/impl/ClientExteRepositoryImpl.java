package com.muigapps.curso2022.proyecto.core.repository.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;

import com.muigapps.curso2022.proyecto.core.model.Client;
import com.muigapps.curso2022.proyecto.core.repository.ClientExteRepository;



@Repository
public class ClientExteRepositoryImpl implements ClientExteRepository{

	

	@PersistenceContext
	EntityManager em;
	
	@Override
	public List<Client> findBYName(String name) {
		String sql = "select c from Client c where c.name like '%"+name+"%'";
		
		return em.createQuery(sql).getResultList();
	}

}
