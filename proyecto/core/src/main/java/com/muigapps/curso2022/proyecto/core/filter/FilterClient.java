package com.muigapps.curso2022.proyecto.core.filter;

import java.util.Date;

public class FilterClient {
	
	private String name;
	private String lastname;
	private String vat;
	private Date fromDate;
	private Date toDate;
	private String city;
	private String postalCode;
	private Date appointmentFom;
	private Date appointmentTo;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getLastname() {
		return lastname;
	}
	public void setLastname(String lastname) {
		this.lastname = lastname;
	}
	public String getVat() {
		return vat;
	}
	public void setVat(String vat) {
		this.vat = vat;
	}
	public Date getFromDate() {
		return fromDate;
	}
	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}
	public Date getToDate() {
		return toDate;
	}
	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public Date getAppointmentFom() {
		return appointmentFom;
	}
	public void setAppointmentFom(Date appointmentFom) {
		this.appointmentFom = appointmentFom;
	}
	public Date getAppointmentTo() {
		return appointmentTo;
	}
	public void setAppointmentTo(Date appointmentTo) {
		this.appointmentTo = appointmentTo;
	}
	public String getPostalCode() {
		return postalCode;
	}
	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}
	
	
	
	

}
