package com.muigapps.curso2022.proyecto.core.jdbc.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.muigapps.curso2022.proyecto.core.model.Appointment;
import com.muigapps.curso2022.proyecto.core.model.Client;

public class AppointmentMapper implements RowMapper<Appointment>{

	
	@Override
	public Appointment mapRow(ResultSet rs, int rowNum) throws SQLException {
		Appointment appointment = new Appointment();
		appointment.setId(rs.getInt("id"));
		appointment.setName(rs.getString("name"));
		appointment.setDate(rs.getDate("date"));
		
		if(rs.getObject("idClient") != null) {
			Client client = new Client();
			client.setId(rs.getInt("idClient"));
			client.setName(rs.getString("nameClient"));
			appointment.setClient(client);
		}
		
		return appointment;
	}

}
