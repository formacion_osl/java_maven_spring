package com.muigapps.curso2022.proyecto.core.manager.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import com.muigapps.curso2022.proyecto.core.jdbc.MedicationJdbc;
import com.muigapps.curso2022.proyecto.core.manager.MedicationManager;
import com.muigapps.curso2022.proyecto.core.model.Client;
import com.muigapps.curso2022.proyecto.core.model.Medication;
import com.muigapps.curso2022.proyecto.core.repository.MedicationRepository;

@Component
public class MedicationManagerImpl implements MedicationManager{

	@Autowired
	private MedicationRepository repository;
	
	@Autowired
	private MedicationJdbc medicationJdbc;
	
	
	@Override
	public Medication findOne(Integer id) {
		return repository.findById(id).orElse(null);
	}

	@Override
	public Medication save(Medication medication) {
		return repository.save(medication);
	}

	@Override
	public List<Medication> findAll() {
		return repository.findAll();
	}

	@Override
	public Boolean delete(Integer id) {
		repository.deleteById(id);
		return true;
	}
	

	@Override
	public Page<Medication> findAll(Integer page, Integer size) {
		return repository.findAll(PageRequest.of(page, size, Sort.by(Direction.DESC, "id")));
	}

}
