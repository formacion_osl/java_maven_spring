package com.muigapps.curso2022.proyecto.core.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.muigapps.curso2022.proyecto.core.model.Rol;

public interface RolRepository extends JpaRepository<Rol, String>{
	

}
