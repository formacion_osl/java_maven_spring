package com.muigapps.curso2022.proyecto.core.model;

import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.muigapps.curso2022.proyecto.core.jaxbadapter.DateAdapter;

@Entity
@Table(name = "appointment")
@XmlRootElement
public class Appointment {

	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)	
	private Integer id;

	private String name;
	
//	@XmlElement
//	@XmlJavaTypeAdapter(DateAdapter.class)
	private Date date;
	
	@JsonIgnore
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="client_id")
	private Client client;
	
	
	@ManyToMany
	@JoinTable(name="appointment_medications",
	joinColumns = {
			@JoinColumn(table = "appointment", referencedColumnName = "id")
	},
	inverseJoinColumns =  {
			@JoinColumn(table = "medications", referencedColumnName = "id")
	})
	private List<Medication> medications;

	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Client getClient() {
		return client;
	}

	public void setClient(Client client) {
		this.client = client;
	}

	public List<Medication> getMedications() {
		return medications;
	}

	public void setMedications(List<Medication> medications) {
		this.medications = medications;
	}

	
	
	
	
}
