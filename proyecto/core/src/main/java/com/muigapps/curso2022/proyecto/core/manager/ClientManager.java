package com.muigapps.curso2022.proyecto.core.manager;

import java.util.List;

import org.springframework.data.domain.Page;

import com.muigapps.curso2022.proyecto.core.filter.FilterClient;
import com.muigapps.curso2022.proyecto.core.model.Client;

public interface ClientManager {
	
	Client findOne(Integer id);
	
	Client save(Client client);
	
	List<Client> findAll();
	
	Boolean delete(Integer id);

	Page<Client> findAll(Integer page, Integer size);

	List<Client> byname(String name);

	Page<Client> filter(Integer page, Integer size, FilterClient filterClient);

}
