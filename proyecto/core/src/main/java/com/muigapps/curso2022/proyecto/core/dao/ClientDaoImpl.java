package com.muigapps.curso2022.proyecto.core.dao;

import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.muigapps.curso2022.proyecto.core.jdbc.ClientJdbc;
import com.muigapps.curso2022.proyecto.core.model.Client;
import com.muigapps.curso2022.proyecto.core.model.Street;

@Repository("clientDao")
public class ClientDaoImpl implements ClientJdbc{

	@Autowired
	private EntityManager entityManager;
	
	@Override
	public Client findOne(Integer id) {
		return entityManager.createQuery("select c from Client c where c.id = " + id, Client.class).getResultList().get(0);
	}

	@Override
	public Client save(Client client) {
		entityManager.persist(client);
		return client;
	}

	@Override
	public List<Client> findAll() {
		return entityManager.createQuery("select new Client(c.id, c.name, c.lastname, c.vat, c.birthday, s) from Client c left join c.street s ",Client.class).getResultList();
	}

	@Override
	public Boolean delete(Integer id) {
		entityManager.remove(findOne(id));
		return true;
	}

}
