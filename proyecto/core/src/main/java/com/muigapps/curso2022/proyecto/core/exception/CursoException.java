package com.muigapps.curso2022.proyecto.core.exception;

public class CursoException extends Exception{


	private static final long serialVersionUID = 5245523309429246634L;
	
	
	private String message;


	public CursoException(String message) {
		super(message);
		this.message = message;
	}
	
	public CursoException(String message, Exception e) {
		super(e);
		this.message = message;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	
	
	

}
