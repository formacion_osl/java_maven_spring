package com.muigapps.curso2022.proyecto.core.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.muigapps.curso2022.proyecto.core.model.Appointment;

public interface AppointmentRepository extends JpaRepository<Appointment, Integer>{

	@Query("select a from Appointment a where a.date < :date and a.client.name like :nameClient")
	//@Query("select a from Appointment a inner join a.client c where a.date < :date and c.name = :nameClient")
	List<Appointment> findBeforeDateAndNameClient(@Param("date")Date date, @Param("nameClient")String nameClient);
}
