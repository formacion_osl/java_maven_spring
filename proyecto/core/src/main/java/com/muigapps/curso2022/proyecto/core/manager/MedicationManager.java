package com.muigapps.curso2022.proyecto.core.manager;

import java.util.List;

import org.springframework.data.domain.Page;

import com.muigapps.curso2022.proyecto.core.model.Medication;

public interface MedicationManager {
	
	Medication findOne(Integer id);
	
	Medication save(Medication medication);
	
	List<Medication> findAll();
	
	Boolean delete(Integer id);

	Page<Medication> findAll(Integer page, Integer size);

}
 