package com.muigapps.dia2.copyresult;


import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;

import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

/**
 * Goal which touches a timestamp file.
 */
@Mojo( name = "copyresult", defaultPhase = LifecyclePhase.PACKAGE )
public class Copyresult extends AbstractMojo
{
    /**
     * Location of the file.
     */
    @Parameter( defaultValue = "${project.build.directory}", property = "outputDir", required = true )
    private File outputDirectory;
    
    @Parameter(defaultValue = "${project.build.directory}", property = "copyDir", required = true)
    private File copyDirectory;
    
    @Parameter(property = "extensions", required = true)
    private String[] extensions;

    
    public void execute() throws MojoExecutionException
    {
    
    	getLog().info("COPY RESULT inicia su ejecución");
    	getLog().info("DESDE:  " + outputDirectory.getAbsolutePath());
    	getLog().info("HASTA:  " + copyDirectory.getAbsolutePath());
    	if(extensions != null && extensions.length > 0) {
	    	for(String ext: extensions) {
	        	getLog().info("SE VA A COPIAR LA EXTESION:  " + ext);
	    	}
    	} else {
    		getLog().error("NO SE HAN INDICADO LA EXTSION DEL ARCHIVO");
    	}
    	
    	
    	if(outputDirectory != null && extensions != null && extensions.length > 0) {
    		
    		ArrayList<String> listExtensions = new ArrayList<String>();
    		for(String ext: extensions) {
    			listExtensions.add(ext);
	    	}
    		
    		File fOut = copyDirectory;
    		
    		if(!fOut.exists()) {
    			fOut.mkdirs();
    		}
    		
    		if(!outputDirectory.exists()) {
    			throw new MojoExecutionException("No existe el directorio desde el que copiar");
    		}
    		
    		for(final File fileEntry: outputDirectory.listFiles()) {
    			
    			if(!fileEntry.isDirectory()) {
    				String extension = fileEntry.getName().substring(fileEntry.getName().lastIndexOf(".") + 1);
    				if(listExtensions.contains(extension)) {
    					File newFile = new File(fOut,fileEntry.getName());
    					
    					try {
							FileUtils.copyFile(fileEntry, newFile);
						} catch (IOException e) {
							throw new MojoExecutionException("Error al copiar el fichero");
						}
    				}
    			}
    			
    		}
    		
    		
    		
    	}
    	
    }
}
